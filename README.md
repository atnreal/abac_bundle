Atnreal ABAC Bundle
=================

Installation
============

### Step 1: Download the Bundle

Add in your project `composer.json` file:

```json
{
    "require": {
    //...
    "atnreal/abac_bundle": "*"
    },
    "minimum-stability": "dev",
    "prefer-stable" : true,
    "repositories": [
        {
            "type": "vcs",
            "url":  "git@bitbucket.org:atnreal/abac_bundle.git"
        }
    ]
}
```

Open a command console, enter your project directory and execute the
following command to download the latest version of this bundle:

```console
$ composer update
```

This command requires you to have Composer installed globally, as explained
in the [installation chapter](https://getcomposer.org/doc/00-intro.md)
of the Composer documentation.

### Step 2: Enable the Bundle

Then, enable the bundle by adding it to the list of registered bundles
in the `app/AppKernel.php` file of your project:

```php
<?php
// app/AppKernel.php

// ...
class AppKernel extends Kernel
{
    public function registerBundles()
    {
        $bundles = array(
            // ...
            new Atnreal\AbacBundle\AbacBundle(),
        );

        // ...
    }

    // ...
}
```
### Step 3: Add cache container description to `service.yaml`

Add to `service.yaml` `abac_bundle.cache` container description. RedisCache for example:

```yaml
abac_bundle.cache:
        class: Doctrine\Common\Cache\RedisCache
        calls:
        - [setRedis, ['@snc_redis.default']]
```

### Step 4: Set authenticator
            
Add token_authenticator as authenticator to `security.yaml`:

```yaml

// ...
 guard:
       authenticators:
            abac_bundle.token_authenticator
// ...           
```

### Step 5: Using of User entity

Use entity of user in your voters:

```php
// ...
use Atnreal\AbacBundle\Security\User;
// ... 

    protected function voteOnAttribute($attribute, $subject, TokenInterface $token)
    {
        $user = $token->getUser();
        
        // ...
    }
// ...
```