<?php

namespace Atnreal\AbacBundle\DataProvider;

use ApiPlatform\Core\DataProvider\ItemDataProviderInterface;
use ApiPlatform\Core\DataProvider\RestrictedDataProviderInterface;
use Atnreal\AbacBundle\Entity\Cache;

class CacheItemDataProvider implements RestrictedDataProviderInterface, ItemDataProviderInterface
{
    public function supports(string $resourceClass, string $operationName = null, array $context = []): bool
    {
        return Cache::class === $resourceClass;
    }

    public function getItem(string $resourceClass, $id, string $operationName = null, array $context = [])
    {
        return new Cache($id);
    }
}
