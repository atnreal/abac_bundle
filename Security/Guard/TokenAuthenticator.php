<?php

namespace Atnreal\AbacBundle\Security\Guard;

use Doctrine\Common\Cache\CacheProvider;
use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use GuzzleHttp\Exception\ClientException;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Guard\AbstractGuardAuthenticator;

class TokenAuthenticator extends AbstractGuardAuthenticator
{
    public const ACCESS_TOKEN_CACHE_PREFIX = 'accessToken_';

    /**
     * @var CacheProvider
     */
    private $cache;

    /**
     * @var ClientInterface|Client
     */
    private $client;

    /**
     * @var string
     */
    private $authApiDsn;

    /**
     * @param CacheProvider $cache
     * @param ClientInterface|Client $client
     * @param string $authApiDsn
     */
    public function __construct(CacheProvider $cache, ClientInterface $client, string $authApiDsn)
    {
        $this->cache = $cache;
        $this->client = $client;
        $this->authApiDsn = $authApiDsn;
    }

    /**
     * Returns a response that directs the user to authenticate.
     *
     * This is called when an anonymous request accesses a resource that
     * requires authentication. The job of this method is to return some
     * response that "helps" the user start into the authentication process.
     *
     * Examples:
     *  A) For a form login, you might redirect to the login page
     *      return new RedirectResponse('/login');
     *  B) For an API token authentication system, you return a 401 response
     *      return new Response('Auth header required', 401);
     *
     * @param Request $request The request that resulted in an AuthenticationException
     * @param AuthenticationException $authException The exception that started the authentication process
     *
     * @return Response
     */
    public function start(Request $request, AuthenticationException $authException = null)
    {
        $data = [
            'message' => 'Authentication Required'
        ];

        return new JsonResponse($data, Response::HTTP_UNAUTHORIZED);
    }

    /**
     * Does the authenticator support the given Request?
     *
     * If this returns false, the authenticator will be skipped.
     *
     * @param Request $request
     *
     * @return bool
     */
    public function supports(Request $request)
    {
        return $request->headers->has('X-AUTH-TOKEN') || $request->request->has('x-auth-token');
    }

    /**
     * Get the authentication credentials from the request and return them
     * as any type (e.g. an associate array).
     *
     * Whatever value you return here will be passed to getUser() and checkCredentials()
     *
     * For example, for a form login, you might:
     *
     *      return array(
     *          'username' => $request->request->get('_username'),
     *          'password' => $request->request->get('_password'),
     *      );
     *
     * Or for an API token that's on a header, you might use:
     *
     *      return array('api_key' => $request->headers->get('X-API-TOKEN'));
     *
     * @param Request $request
     *
     * @return mixed Any non-null value
     *
     * @throws \UnexpectedValueException If null is returned
     */
    public function getCredentials(Request $request)
    {
        if ($request->headers->has('X-AUTH-TOKEN')) {
            return [
                'access_token' => $request->headers->get('X-AUTH-TOKEN'),
            ];
        }

        return [
            'access_token' => $request->request->get('x-auth-token'),
        ];
    }

    /**
     * Return a UserInterface object based on the credentials.
     *
     * The *credentials* are the return value from getCredentials()
     *
     * You may throw an AuthenticationException if you wish. If you return
     * null, then a UsernameNotFoundException is thrown for you.
     *
     * @param mixed $credentials
     * @param UserProviderInterface $userProvider
     *
     * @throws AuthenticationException
     *
     * @return \Atnreal\AbacBundle\Security\UserInterface|null
     */
    public function getUser($credentials, UserProviderInterface $userProvider)
    {
        $accessToken = $credentials['access_token'] ?? null;

        try {
            $userData = $this->getUserDataByAccessToken($accessToken);
        } catch (\Exception $exception) {
            throw new HttpException(401, $exception->getMessage());
        }

        $attributes = $userData['user']['attributes'] ?? [];
        $id = $userData['user']['id'] ?? '';
        $username = $userData['user']['username'] ?? '';

        /** @var \Atnreal\AbacBundle\Security\UserInterface $user */
        $user = $userProvider->loadUserByUsername($id);

        $user->setAttributes($attributes);
        $user->setUsername($username);

        return $user;
    }

    /**
     * @param string $accessToken
     *
     * @return array
     */
    private function getUserDataByAccessToken(string $accessToken): array
    {
        $this->cache->setNamespace(self::ACCESS_TOKEN_CACHE_PREFIX);

        if ($this->cache->contains($accessToken)) {
            return $this->cache->fetch($accessToken);
        }

        try {
            $response = $this->client->get($this->authApiDsn . '/api/user_data/' . $accessToken);
        } catch (ClientException $exception) {
            $response = json_decode($exception->getResponse()->getBody()->getContents(), true);

            throw new HttpException(401, $response['message']);
        }

        $responseArray = json_decode($response->getBody(), true);

        $userData = $responseArray['data'] ?? [];
        $accessTokenExpiredAt = $userData['accessTokenExpiredAt'] ?? null;

        $accessTokenExpiredAt = new \DateTime($accessTokenExpiredAt);
        $this->saveDataIntoCache($accessToken, $accessTokenExpiredAt, $userData);

        return $userData;
    }


    /**
     * @param string $accessToken
     * @param \DateTime $accessTokenExpiredAt
     * @param array $userData
     */
    private function saveDataIntoCache(string $accessToken, \DateTime $accessTokenExpiredAt, array $userData): void
    {
        $cacheTtl = $accessTokenExpiredAt->getTimestamp() - (new \DateTime())->getTimestamp();

        if ($cacheTtl > 0) {
            $this->cache->save($accessToken, $userData, $cacheTtl);
        }
    }

    /**
     * Returns true if the credentials are valid.
     *
     * If any value other than true is returned, authentication will
     * fail. You may also throw an AuthenticationException if you wish
     * to cause authentication to fail.
     *
     * The *credentials* are the return value from getCredentials()
     *
     * @param mixed $credentials
     * @param UserInterface $user
     *
     * @return bool
     *
     * @throws AuthenticationException
     */
    public function checkCredentials($credentials, UserInterface $user)
    {
        return true;
    }

    /**
     * Called when authentication executed, but failed (e.g. wrong username password).
     *
     * This should return the Response sent back to the user, like a
     * RedirectResponse to the login page or a 403 response.
     *
     * If you return null, the request will continue, but the user will
     * not be authenticated. This is probably not what you want to do.
     *
     * @param Request $request
     * @param AuthenticationException $exception
     *
     * @return Response|null
     */
    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = [
            'message' => strtr($exception->getMessageKey(), $exception->getMessageData())
        ];

        return new JsonResponse($data, Response::HTTP_FORBIDDEN);
    }

    /**
     * Called when authentication executed and was successful!
     *
     * This should return the Response sent back to the user, like a
     * RedirectResponse to the last page they visited.
     *
     * If you return null, the current request will continue, and the user
     * will be authenticated. This makes sense, for example, with an API.
     *
     * @param Request $request
     * @param TokenInterface $token
     * @param string $providerKey The provider (i.e. firewall) key
     *
     * @return Response|null
     */
    public function onAuthenticationSuccess(Request $request, TokenInterface $token, $providerKey)
    {
        return null;
    }

    /**
     * Does this method support remember me cookies?
     *
     * Remember me cookie will be set if *all* of the following are met:
     *  A) This method returns true
     *  B) The remember_me key under your firewall is configured
     *  C) The "remember me" functionality is activated. This is usually
     *      done by having a _remember_me checkbox in your form, but
     *      can be configured by the "always_remember_me" and "remember_me_parameter"
     *      parameters under the "remember_me" firewall key
     *  D) The onAuthenticationSuccess method returns a Response object
     *
     * @return bool
     */
    public function supportsRememberMe()
    {
        return false;
    }
}
