<?php

namespace Atnreal\AbacBundle\Security;

interface UserInterface
{
    /**
     * @param array $attributes
     * @return mixed
     */
    public function setAttributes(array $attributes);

    /**
     * @return mixed
     */
    public function getAttributes();

    /**
     * @param string $username
     * @return mixed
     */
    public function setUsername($username);
}
