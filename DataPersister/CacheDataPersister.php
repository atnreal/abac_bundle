<?php

namespace Atnreal\AbacBundle\DataPersister;

use ApiPlatform\Core\DataPersister\DataPersisterInterface;
use Atnreal\AbacBundle\Entity\Cache;
use Atnreal\AbacBundle\Security\Guard\TokenAuthenticator;
use Doctrine\Common\Cache\CacheProvider;

class CacheDataPersister implements DataPersisterInterface
{
    /**
     * @var CacheProvider
     */
    private $cache;

    /**
     * @param CacheProvider $cache
     */
    public function __construct(CacheProvider $cache)
    {
        $this->cache = $cache;
        $this->cache->setNamespace(TokenAuthenticator::ACCESS_TOKEN_CACHE_PREFIX);
    }

    /**
     * Is the data supported by the persister?
     *
     * @param $data
     * @return bool
     */
    public function supports($data): bool
    {
        return $data instanceof Cache;
    }

    /**
     * Persists the data.
     *
     * @param Cache $data
     * @return object|void Void will not be supported in API Platform 3, an object should always be returned
     */
    public function persist($data)
    {
        throw new \LogicException('This code must not be reached.');
    }

    /**
     * Removes the data.
     *
     * @param Cache $data
     */
    public function remove($data)
    {
        $this->cache->delete($data->getKey());
    }
}